﻿using UnityEngine;

public class ItemPickup : Interactable
{
    public Item item;

    public override void InteractAction()
    {
        // base.InteractAction();
        PickUp();
    }

    public override void NonInteractAction()
    {
        // base.NonInteractAction();
    }

    void PickUp()
    {
        if(item.isActive)
        {
            Debug.Log("Picking up " + item.name);
            bool hasPickUp = Inventory.instance.Add(item);

            if(hasPickUp)
            {
                Destroy(gameObject);
            }
        }
        
        else
        {
            Debug.Log("Item not pickable");
        }
    }
}
