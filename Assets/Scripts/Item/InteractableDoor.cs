﻿using UnityEngine;

public class InteractableDoor : Interactable
{
    public GameObject domeDoor;
    private Animator doorAnimation;

    void Start()
    {
        doorAnimation = domeDoor.GetComponent<Animator>();
    }

    public override void InteractAction()
    {
        // base.InteractAction();
        if (doorAnimation != null)
        {
            doorAnimation.ResetTrigger("CloseTrigger");
            doorAnimation.SetTrigger("OpenTrigger");
        }
    }

    public override void NonInteractAction()
    {
        //  base.NonInteractAction();
        if (doorAnimation != null)
        {
            doorAnimation.ResetTrigger("OpenTrigger");
            doorAnimation.SetTrigger("CloseTrigger");
        }
    }
}