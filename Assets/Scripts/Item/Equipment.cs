﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "New Equipment", menuName = "Inventory/Equipment")]
public class Equipment : Item
{
    [Header("Configuration")]
    public EquipmentSlot equipSlot;

    [Header("Stat Setting")]
    public float hitPoint = 0f;
    public float weaponDef = 0f;

    [Header("Weapon Setting")]
    public bool isWeapon = false;
    public bool isRanged = false;
    [HideInInspector] public float damage = 0f;
    [HideInInspector] public float range = 100f;
    [HideInInspector] public float fireRate = 5f;
    [HideInInspector] public float impactForce = 30f;

    public override void Select()
    {
        base.Select();
        EquipmentManager.instance.ActiveButton(this);
        // EquipmentManager.instance.Equip(this);
        // RemoveFromInventory();
    }
}

public enum EquipmentSlot{Head, Chest, Arms, Legs, Weapon}

#if UNITY_EDITOR
[CustomEditor(typeof(Equipment))]
public class Equipment_Editor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector(); // for other non-HideInInspector fields

        Equipment script = (Equipment)target;

        if (script.isWeapon) // if bool is true, show other fields
        {
            script.damage = EditorGUILayout.FloatField("Damage", script.damage);

            if (script.isRanged)
            {
                script.range = EditorGUILayout.FloatField("Range", script.range);
                script.fireRate = EditorGUILayout.FloatField("Fire Rate", script.fireRate);
                script.impactForce = EditorGUILayout.FloatField("Impact Force", script.impactForce);
            }
        }
    }
}
 #endif