﻿using UnityEngine;

public class Item : ScriptableObject
{
    new public string name = "Item";
    public string description;
    public ItemType type;
    public Sprite icon = null;
    public GameObject modelPrefab;
    public bool isActive = true;
    public bool disposable = true;

    public virtual void Select()
    {
        Debug.Log(name + " is selected");
    }

    public void RemoveFromInventory()
    {
        Inventory.instance.Remove(this);
    }
}

public enum ItemType{UseItem, Equipment}

