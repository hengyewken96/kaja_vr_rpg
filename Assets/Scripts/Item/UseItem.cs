﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "New Use Item", menuName = "Inventory/Use Item")]
public class UseItem : Item
{
    [Header("Configuration")]
    public UseItemSlot useSlot;

    [Header("Effect Setting")]
    [HideInInspector] public float heal = 0f;
    [HideInInspector] public float damage = 0f;
    [HideInInspector] public float hitPoint = 0f;
    [HideInInspector] public float weaponDef = 0f;

    public override void Select()
    {
        base.Select();
        UseItemManager.instance.ActiveButton(this);
        // UseItemManager.instance.ApplyEffect(this);
        // RemoveFromInventory();
    }
}

public enum UseItemSlot{Heal, Boost}

#if UNITY_EDITOR
[CustomEditor(typeof(UseItem))]
public class UseItem_Editor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector(); // for other non-HideInInspector fields

        UseItem script = (UseItem)target;
        
        switch(script.useSlot)
        {
            case UseItemSlot.Heal: 
                script.heal = EditorGUILayout.FloatField("Heal amount", script.heal);
                break;
            case UseItemSlot.Boost:
                script.damage = EditorGUILayout.FloatField("Increase Damage", script.damage);
                script.hitPoint = EditorGUILayout.FloatField("Increase Max Health", script.hitPoint);
                script.weaponDef = EditorGUILayout.FloatField("Increse Weapon Def", script.weaponDef);
                break;
            default:
                script.heal = EditorGUILayout.FloatField("Heal amount", script.heal);
                break;
        }
    }
}
 #endif
