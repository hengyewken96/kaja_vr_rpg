﻿using UnityEngine;

public class DialogNPC : Interactable
{
    public GameObject icon;
    public GameObject dialog;

    public override void InteractAction()
    {
        // base.InteractAction();
        icon.SetActive(false);
        dialog.SetActive(true);
    }

    public override void NonInteractAction()
    {
        //  base.NonInteractAction();
        icon.SetActive(true);
        dialog.SetActive(false);
    }
}