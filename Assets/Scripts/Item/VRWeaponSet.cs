﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class VRWeaponSet : MonoBehaviour
{
    [Header("Gun Default")]
    public CharacterStats characterStats;
    public Camera muzzleHead;
    public ParticleSystem muzzleFlash;
    public GameObject impactEffect;

    [Header("Input Setting")]
    private float impactForce;
    private float range;
    private float fireRate;
    private float nextTimeToFire = 0f;
    private GameObject weaponPrefab;
    private StatManager statManager;
    private EquipmentManager equipmentManager;

    private SteamVR_Behaviour_Pose pose = null;
    public SteamVR_Action_Boolean fire = null;
    
    private void Awake()
    {
        pose = GetComponentInParent<SteamVR_Behaviour_Pose>();
        statManager = StatManager.instance;
        equipmentManager = EquipmentManager.instance;
    }

    void Start()
    {
        equipmentManager.onEquipmentChanged += OnEquipmentChanged;
    }

    void Update()
    {
        if(fire.GetState(pose.inputSource) && Time.time >= nextTimeToFire)
        {
            if(weaponPrefab != null)
            {
                nextTimeToFire = Time.time + (1f / fireRate);
                Shoot();
            }
        }
    }

    void Shoot()
    {
        muzzleFlash.Play();
        RaycastHit hit;
        
        if (Physics.Raycast(muzzleHead.transform.position, muzzleHead.transform.forward, out hit, range))
        {
            EnemyAI target = hit.transform.GetComponent<EnemyAI>();
            if(target != null)
            {
                // Debug.Log(hit.transform.name);
                target.ShootDamage(statManager.GetDamageValue());
            }

            if(hit.rigidbody != null)
            { 
                hit.rigidbody.AddForce(-hit.normal * impactForce);
            }

            GameObject impact = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(impact, 1f);
        }
    }

    void OnEquipmentChanged(Equipment newItem, Equipment oldItem)
    {
        if(newItem != null && newItem.isWeapon && newItem.isRanged)
        {
            impactForce = newItem.impactForce;
            range = newItem.range;
            fireRate = newItem.fireRate;
            InstantiateRangedWeapon(newItem);
        }

        else if(oldItem != null && oldItem.isWeapon && oldItem.isRanged)
        {
            Destroy(weaponPrefab);
        }
    }

    void InstantiateRangedWeapon(Equipment equip)
    {
        weaponPrefab = Instantiate(equip.modelPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
        weaponPrefab.transform.parent = GameObject.Find("Ranged").transform;
        weaponPrefab.transform.localPosition = Vector3.zero;
        weaponPrefab.transform.localRotation = Quaternion.identity;
    }
}
