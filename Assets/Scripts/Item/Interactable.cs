﻿using UnityEngine;

public class Interactable : MonoBehaviour
{
    private Transform player;
    private Vector3 closestSurfacePoint;
    private float radius;
    private bool isInteractable = false;
    private bool hasInteractable = false;

    public virtual void InteractAction()
    {
        Debug.Log("Interacted with " + gameObject.name);
    }

    public virtual void NonInteractAction()
    {
        Debug.Log(gameObject.name + " is now outside the scan radius");
    }

    void Update()
    {
        if(isInteractable)
        {
            closestSurfacePoint = gameObject.GetComponent<Collider>().ClosestPointOnBounds(player.position);
            float distance = Vector3.Distance(closestSurfacePoint, player.position);
            if(distance > radius)
            {
                NonInteractAction();
                isInteractable = false;
            }

            if(distance <= 1.0)
            {
                InteractAction();
                hasInteractable = true;
            }
        }
    }

    public void Interact(Transform playerTransform, float scanRadius)
    {
        if(!isInteractable)
        {
            // Debug.Log(gameObject.name + " detected");
            isInteractable = true;
            player = playerTransform;
            radius = scanRadius;
        }
    }
}
