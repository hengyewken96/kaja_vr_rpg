﻿using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    public Transform inventoryPanel;
    public int typeCode;
    Inventory inventory;
    InventorySlot[] slots;

    void Start()
    {
        inventory = Inventory.instance;
        inventory.onItemChangedCallback += UpdateUI;
        slots = inventoryPanel.GetComponentsInChildren<InventorySlot>();
    }

    void UpdateUI()
    {
        for(int i = 0; i < slots.Length; i++)
        {
            if (i < inventory.listItem[typeCode].Count)
            {
                slots[i].AddItem(inventory.listItem[typeCode][i]);
            }

            else
            {
                slots[i].ClearSlot();
            }
        }
    }
}
