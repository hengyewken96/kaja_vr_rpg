﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    #region Singleton
    public static Inventory instance; // Create a static instance to ensure that only one inventory existed throughout the game

    void Awake()
    {
        if(instance != null)
        {
            Debug.LogWarning("More than one inventory instance found");
            return;
        }

        instance = this; // Reference the instance to the current inventory
    }
    #endregion

    public int inventorySpace = 24;
    public List<List<Item>> listItem = new List<List<Item>>();
    public List<Item> equipmentList = new List<Item>();
    public List<Item> useItemList = new List<Item>();

    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    private int typeCode;

    void Start()
    {
        listItem.Add(equipmentList);
        listItem.Add(useItemList);
    }

    // Add item to the inventory
    public bool Add(Item item)
    {
        InventoryType(item.type);
        if(item.isActive)
        {
            // Check whether there is sufficient space for inventory
            if(listItem[typeCode].Count >= inventorySpace)
            {
                Debug.Log("The inventory is full");
                return false;
            }

            listItem[typeCode].Add(item);
            // Debug.Log(item.name + " is added to the inventory");

            // Trigger callback event
            if(onItemChangedCallback != null)
            {
                onItemChangedCallback.Invoke();
            }
        }

        return true;
    }

    // Remove item from the inventory
    public void Remove(Item item)
    {
        InventoryType(item.type);
        listItem[typeCode].Remove(item);
        // Debug.Log(item.name + " is removed the inventory");

        // Trigger callback event
        if(onItemChangedCallback != null)
        {
            onItemChangedCallback.Invoke();
        }
    }

    private void InventoryType(ItemType type)
    {
        switch(type)
        {
            case ItemType.Equipment: typeCode = 0; break;
            case ItemType.UseItem: typeCode = 1; break;
            default: Debug.Log("Inventory type does not exist"); break;
        }
    }
}
