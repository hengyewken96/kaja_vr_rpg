﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "NPC/Enemy")]
public class EnemyStat : ScriptableObject
{
    [Header("General")]
    new public string name = "Enemy";
    public GameObject mobPrefab;

    [Header("Stats")]
    public float mobHealth = 50f;
    public float mobDef = 5f;
    public float mobEXP = 15f;

    [Header("Others")]
    public float chaseSpeed = 1.5f;
    public float rotateSpeed = 5.0f;
    public float watchRange = 10.0f;
    public float chaseRange = 8.0f;
    public float stopDistance = 2.0f;
}
