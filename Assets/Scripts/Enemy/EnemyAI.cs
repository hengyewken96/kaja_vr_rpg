﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    [Header("Enemy Setting")]
    public EnemyStat mobInfo;

    private float mobMaxHealth;
    private float mobCurrentHealth;
    private float mobDef;
    private float mobEXP;
    private float chaseSpeed;
    private float rotateSpeed;
    private float watchRange;
    private float chaseRange;
    private float stopDistance;
    private float damage;
    private Transform target;
    private Transform mobTransform;
    private EnemyManager enemyManager;

    private void Awake()
    {
        target = GameObject.FindWithTag("Player").transform;
        mobTransform = transform;
        enemyManager = EnemyManager.instance;
    }

    private void Start()
    {
        if(mobInfo != null)
        {
            mobMaxHealth = mobInfo.mobHealth;
            mobCurrentHealth = mobInfo.mobHealth;
            mobDef = mobInfo.mobDef;
            mobEXP = mobInfo.mobEXP;
            chaseSpeed = mobInfo.chaseSpeed;
            rotateSpeed = mobInfo.rotateSpeed;
            watchRange = mobInfo.watchRange;
            chaseRange = mobInfo.chaseRange;
            stopDistance = mobInfo.stopDistance;
        }
    }

    private void Update()
    {
        var distance = Vector3.Distance(mobTransform.position, target.position);
        
        if(distance <= watchRange)
        {
            Vector3 tempTransform = new Vector3(target.position.x, mobTransform.position.y, target.position.z);
            mobTransform.rotation = Quaternion.Slerp(mobTransform.rotation, Quaternion.LookRotation(tempTransform - mobTransform.position), rotateSpeed * Time.deltaTime);
            
            if(distance <= chaseRange && distance >= stopDistance)
            {
                mobTransform.position += mobTransform.forward * chaseSpeed * Time.deltaTime;
            }
        }
    }

    public void ShootDamage(float amount)
    {
        damage = amount - mobDef;
        damage = Mathf.Clamp(damage, 0, float.MaxValue);
        mobCurrentHealth -= damage;

        // Debug.Log("Mob took " + damage + " damage");
        if(mobCurrentHealth <= 0f)
        {
            TriggerDeath();
        }
    }

    private void TriggerDeath()
    {
        enemyManager.onEnemyTriggerDeath(mobEXP);
        Destroy(gameObject);
    }
}
