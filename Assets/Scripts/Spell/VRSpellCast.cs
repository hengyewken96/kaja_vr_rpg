﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRSpellCast : MonoBehaviour
{
    [Header("Setting")]
    public int spell_Force = 10;
    public int spell_Count = 1;
    public GameObject spell_Prefab = null;
    public Transform spell_Launch = null;

    public Camera mainCam = null;
    private SpellPool spell_Pool = null;
    private int usedSpell = 0;

    private void Awake()
    {
        spell_Pool = new SpellPool(spell_Prefab, spell_Count);
    }

    void Update()
    {
        transform.position = mainCam.transform.position;
        transform.rotation = mainCam.transform.rotation;
    }

    public void SpellCast()
    {
        if(usedSpell >= spell_Count)
        {
            return;
        }
        VRSpell targetProjectile = spell_Pool.spells[usedSpell];
        targetProjectile.Launch(this);
    }
}
