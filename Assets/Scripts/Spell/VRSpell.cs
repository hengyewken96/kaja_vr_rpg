﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRSpell : MonoBehaviour
{
    private int spell_Damage = 2;
    public float spell_Lifetime = 5.0f;
    public GameObject impactEffect;
    private Rigidbody spell_Rigidbody = null;

    private void Awake()
    {
        spell_Rigidbody = GetComponent<Rigidbody>();
        SetInnactive();
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject impact = Instantiate(impactEffect, transform.position, Quaternion.identity);
        SetInnactive();
        Destroy(impact, 2f);
    }

    public void Launch(VRSpellCast spellcast)
    {
        // Position
        transform.position = spellcast.spell_Launch.position;
        transform.rotation = spellcast.spell_Launch.rotation;

        // Activate
        gameObject.SetActive(true);

        // Fire and track
        spell_Rigidbody.AddRelativeForce(Vector3.forward * spellcast.spell_Force, ForceMode.Impulse);
        StartCoroutine(TrackLifetime());
    }

    private IEnumerator TrackLifetime()
    {
        yield return new WaitForSeconds(spell_Lifetime);
        SetInnactive();
    }

    public void SetInnactive()
    {
        spell_Rigidbody.velocity = Vector3.zero;
        spell_Rigidbody.angularVelocity = Vector3.zero;

        gameObject.SetActive(false);
    }
}
