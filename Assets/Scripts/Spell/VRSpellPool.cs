﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRSpellPool
{
    public List<T> Create<T>(GameObject prefab, int count) where T : MonoBehaviour
    {
        // New list
        List<T> newPool = new List<T>();

        // Create
        for(int i = 0; i < count; i++)
        {
            GameObject spellObject = GameObject.Instantiate(prefab, Vector3.zero, Quaternion.identity);
            T newSpell = spellObject.GetComponent<T>();

            newPool.Add(newSpell);
        }

        return newPool;
    }
}

public class SpellPool : VRSpellPool
{
    public List<VRSpell> spells = new List<VRSpell>();

    public SpellPool(GameObject prefab, int count)
    {
        spells = Create<VRSpell>(prefab, count);
    }

    public void SetAllProjectiles()
    {
        foreach(VRSpell cast in spells)
        {
            cast.SetInnactive();
        }
    }
}