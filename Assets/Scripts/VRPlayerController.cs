﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class VRPlayerController : MonoBehaviour
{
    public float k_Gravity = 30.0f;
    public float k_Sensitivity = 0.5f;
    public float k_MaxSpeed = 1.5f;
    public float k_RotateIncrement = 90;

    public SteamVR_Action_Boolean k_RotatePress = null;
    public SteamVR_Action_Vector2 k_MoveValue = null;

    private float k_Speed = 0.0f;

    private CharacterController k_CharacterController = null;
    private Transform k_Camera = null;
    private Transform k_Head = null;


    private void Awake()
    {
        k_CharacterController = GetComponent<CharacterController>();
    }

    private void Start()
    {
        k_Camera = SteamVR_Render.Top().origin;
        k_Head = SteamVR_Render.Top().head;
    }

    private void Update()
    {
        HandleHeight();
        CalculateMovement();
        SnapRotation();
    }

    private void HandleHeight()
    {
        // Local space of head
        float headHeight = Mathf.Clamp(k_Head.localPosition.y, 1, 2);
        k_CharacterController.height = headHeight;

        // Half height for center
        Vector3 newCenter = Vector3.zero;
        newCenter.y = k_CharacterController.height / 2;
        newCenter.y += k_CharacterController.skinWidth; 

        // Local space capsule movement
        newCenter.x = k_Head.localPosition.x;
        newCenter.z = k_Head.localPosition.z;

        // Apply
        k_CharacterController.center = newCenter;
    }

    private void CalculateMovement()
    {
        // Movement orientation
        Quaternion orientation = CalculateOrientation();
        Vector3 movement = Vector3.zero;

        // Idle position
        if(k_MoveValue.axis.magnitude == 0)
        {
            k_Speed = 0;
        }

        // Add & clamp
        k_Speed += k_MoveValue.axis.magnitude * k_Sensitivity;
        k_Speed = Mathf.Clamp(k_Speed, -k_MaxSpeed, k_MaxSpeed);

        // Orientation & Gravity
        movement += orientation * (k_Speed * Vector3.forward);
        movement.y -= k_Gravity * Time.deltaTime;

        // Apply
        k_CharacterController.Move(movement * Time.deltaTime);
    }

    private Quaternion CalculateOrientation()
    {
        float rotation = Mathf.Atan2(k_MoveValue.axis.x, k_MoveValue.axis.y);
        rotation *= Mathf.Rad2Deg;

        Vector3 orientationEuler = new Vector3(0, k_Head.eulerAngles.y + rotation, 0);
        return Quaternion.Euler(orientationEuler);
    }

    private void SnapRotation()
    {
        float snapValue = 0.0f;

        if(k_RotatePress.GetStateDown(SteamVR_Input_Sources.LeftHand))
        {
            snapValue = -Mathf.Abs(k_RotateIncrement);
        }

        if(k_RotatePress.GetStateDown(SteamVR_Input_Sources.RightHand))
        {
            snapValue = Mathf.Abs(k_RotateIncrement);
        }

        transform.RotateAround(k_Head.position, Vector3.up, snapValue);
    }
}
