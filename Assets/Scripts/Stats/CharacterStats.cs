using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    [Header("Health Setting")]
    public HealthBar healthBar;
    public EXPBar expBar;
    public float baseHealth = 100f;
    public float currentHealth { get; set; }
    
    [Header("Stat Setting")]
    public Stat dmg;
    public Stat hp;
    public Stat armor;
    public Stat exp;

    private float maxHealth;
    private float damage;
    private float tempEXP;
    private float level;
    private StatManager statManager;

    void Awake()
    {
        maxHealth = baseHealth + hp.GetValue();
        currentHealth = maxHealth;
        healthBar.InitHealth(maxHealth);
        expBar.InitEXP();
        statManager = StatManager.instance;
    }

    void Update()
    {
        if(maxHealth != baseHealth + hp.GetValue())
        {
            maxHealth = baseHealth + hp.GetValue();
            healthBar.SetMaxHealth(maxHealth);
        }

        if(currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        if(Input.GetKeyDown(KeyCode.T))
        {
            TakeDamage(10f);
        }

        level = Mathf.Floor(exp.GetValue() / 100) + 1;
        tempEXP = exp.GetValue() % 100;
        expBar.SetEXP(tempEXP);
        statManager.SetStat(currentHealth, maxHealth, armor.GetValue(), dmg.GetValue(), tempEXP, exp.GetValue(), level);
    }

    public void Heal(float heal)
    {
        currentHealth += heal;
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
        healthBar.SetHealth(currentHealth);
    }

    public void TakeDamage(float dmgTaken)
    {
        damage = dmgTaken - armor.GetValue();
        damage = Mathf.Clamp(damage, 0, float.MaxValue);

        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);
        Debug.Log(transform.name + " takes " + damage + " damage ");

        if (currentHealth <= 0)
        {
            currentHealth = 0;
            TriggerDeath();
        }
    }

    public virtual void TriggerDeath()
    {
        Debug.Log(transform.name + " died");
    }
}