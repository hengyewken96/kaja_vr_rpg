﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : CharacterStats
{
    void Start()
    {
        EquipmentManager.instance.onEquipmentChanged += OnEquipmentChanged;
        UseItemManager.instance.onItemUsed += OnItemUsed;
        EnemyManager.instance.onEnemyTriggerDeath += OnEnemyTriggerDeath;
    }

    void OnEquipmentChanged(Equipment newItem, Equipment oldItem)
    {
        if(newItem != null)
        {
            dmg.AddModifier(newItem.damage);
            hp.AddModifier(newItem.hitPoint);
            armor.AddModifier(newItem.weaponDef);
        }

        if(oldItem != null)
        {
            dmg.RemoveModifier(oldItem.damage);
            hp.RemoveModifier(oldItem.hitPoint);
            armor.RemoveModifier(oldItem.weaponDef);
        }
    }

    void OnItemUsed(UseItem usedItem)
    {
        switch(usedItem.useSlot)
        {
            case UseItemSlot.Heal:
                Heal(usedItem.heal);
                break;
            case UseItemSlot.Boost:
                dmg.AddModifier(usedItem.damage);
                hp.AddModifier(usedItem.hitPoint);
                armor.AddModifier(usedItem.weaponDef);
                break;
            default:
                break;
        }
    }

    void OnEnemyTriggerDeath(float expGain)
    {
        exp.AddModifier(expGain);
    }
}
