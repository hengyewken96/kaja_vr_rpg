﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EXPBar : MonoBehaviour
{
    public Slider slider;
    
    public void InitEXP()
    {
        slider.value = 0;
        slider.maxValue = 100;
    }

    public void SetEXP(float expGain)
    {
        slider.value = expGain;
    }
}
