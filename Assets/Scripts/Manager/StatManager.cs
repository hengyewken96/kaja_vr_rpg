﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatManager : MonoBehaviour
{
    #region Singleton
    public static StatManager instance;

    void Awake()
    {
        if(instance != null)
        {
            Debug.LogWarning("More than one instance found");
            return;
        }

        instance = this;
    }
    #endregion

    [Header("Stat Display Setting")]
    public Text charName;
    public Text charLvl;
    public Text hpVal;
    public Text expVal;
    public Text defVal;
    public Text atkVal;
    public Text headVal;
    public Text chestVal;
    public Text armsVal;
    public Text legsVal;
    public Text weaponVal;

    private float playerMaxHP;
    private float playerMaxDef;
    private float playerMaxAtk;
    private float playerMaxEXP;
    private float playerMaxLvl;

    public void SetName(string name)
    {
        charName.text = name;
    }

    public void SetStat(float currentHP, float maxHP, float armor, float dmg, float currentEXP, float maxEXP, float level)
    {
        // Set stat in text
        hpVal.text = currentHP.ToString() + " / " + maxHP.ToString();
        expVal.text = currentEXP.ToString() + " / 100";
        defVal.text = armor.ToString();
        atkVal.text = dmg.ToString();
        charLvl.text = "Lvl. " + level.ToString();

        // Set stat for global use
        playerMaxHP = maxHP;
        playerMaxDef = armor;
        playerMaxAtk = dmg;
        playerMaxEXP = maxEXP;
        playerMaxLvl = level;
    }

    public void SetEquipment(Equipment[] currentEquipment)
    {
        headVal.text = currentEquipment[0] != null ? currentEquipment[0].name : "None";
        chestVal.text = currentEquipment[1] != null ? currentEquipment[1].name : "None";
        armsVal.text = currentEquipment[2] != null ? currentEquipment[2].name : "None";
        legsVal.text = currentEquipment[3] != null ? currentEquipment[3].name : "None";
        weaponVal.text = currentEquipment[4] != null ? currentEquipment[4].name : "None";
    }

    public float GetDamageValue()
    {
        return playerMaxAtk;
    }
}
