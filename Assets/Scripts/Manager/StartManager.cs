﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartManager : MonoBehaviour
{
    [Header("Menu List")]
    public GameObject startMenu;
    public GameObject genderMenu;
    public GameObject gestureMenu;
    public GameObject baseDoor;
    private Animator doorAnimation;
    private StatManager statManager;
    private CanvasManager canvasManager;
    private bool startGame = false;
    private int gender;

    void Awake()
    {
        doorAnimation = baseDoor.GetComponent<Animator>();
        statManager = StatManager.instance;
        canvasManager = CanvasManager.instance;
    }

    void Start()
    {
        startMenu.SetActive(true);
        genderMenu.SetActive(false);
        gestureMenu.SetActive(false);
    }

    public void StartGame()
    {
        startMenu.SetActive(false);
        genderMenu.SetActive(true);
    }

    public void EndGame()
    {
        Application.Quit();
    }

    public void SelectGender(int value)
    {
        switch(value)
        {
            case 0:
                gender = 0;
                statManager.SetName("O'Niel");
                break;
            case 1:
                gender = 1;
                statManager.SetName("Luna");
                break;
            default:
                break;
        }

        startGame = true;
        genderMenu.SetActive(false);
        gestureMenu.SetActive(true);
        canvasManager.GameStart();

        if (doorAnimation != null)
        {
            // doorAnimation.Play("Base Layer.DoorOpen", 0, 0.25f);
            doorAnimation.SetTrigger("DoorTrigger");
        }

        else
        {
            Debug.Log("no animation found");
        }
    }

    public int ReturnGender()
    {
        return gender;
    }
}
