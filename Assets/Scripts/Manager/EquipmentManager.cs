﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentManager : MonoBehaviour
{
    #region Singleton
    public static EquipmentManager instance;

    void Awake()
    {
        if(instance != null)
        {
            Debug.LogWarning("More than one instance found");
            return;
        }

        instance = this;
    }
    #endregion

    [Header("Active Item Setting")]
    public Text activeText;
    public Text activeDescription;
    public Button activeButton;
    private Equipment activeItem = null;

    private Equipment[] currentEquipment;
    private Inventory inventory;
    private StatManager statManager;
    private Equipment oldItem = null;

    public delegate void OnEquipmentChanged(Equipment newItem, Equipment oldItem);
    public OnEquipmentChanged onEquipmentChanged;

    void Start()
    {
        inventory = Inventory.instance;
        statManager = StatManager.instance;
        int numSlots = System.Enum.GetNames(typeof(EquipmentSlot)).Length;
        currentEquipment = new Equipment[numSlots];
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.U))
        {
            UnequipAll();
        }

        statManager.SetEquipment(currentEquipment);
    }

    public void ActiveButton(Equipment equipment)
    {
        activeText.text = equipment.name.ToString();
        activeDescription.text = equipment.description.ToString();
        activeItem = equipment;
        activeButton.interactable = true;
    }

    public void Equip()
    {
        if(activeItem != null)
        {
            int slotIndex = (int)activeItem.equipSlot;

            if(currentEquipment[slotIndex] != null)
            {
                oldItem = currentEquipment[slotIndex];
                inventory.Add(oldItem);
            }

            if(onEquipmentChanged != null)
            {
                onEquipmentChanged.Invoke(activeItem, oldItem);
            }

            currentEquipment[slotIndex] = activeItem;
            Inventory.instance.Remove(activeItem);
            activeText.text = "";
            activeDescription.text = "";
            activeItem = null;
            activeButton.interactable = false;
        }
    }

    public void Unequip(int slotIndex)
    {
        if(currentEquipment[slotIndex] != null)
        {
            oldItem = currentEquipment[slotIndex];
            inventory.Add(oldItem);
            currentEquipment[slotIndex] = null;

            if(onEquipmentChanged != null)
            {
                onEquipmentChanged.Invoke(null, oldItem);
                oldItem = null;
            }
        }
    }

    public void UnequipAll()
    {
        for(int i = 0; i < currentEquipment.Length; i++)
        {
            Unequip(i);
        }
    }
}
