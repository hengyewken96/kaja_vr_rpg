﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    #region Singleton
    public static EnemyManager instance;

    void Awake()
    {
        if(instance != null)
        {
            Debug.LogWarning("More than one instance found");
            return;
        }

        instance = this;
    }
    #endregion

    public delegate void OnEnemyTriggerDeath(float expGain);
    public OnEnemyTriggerDeath onEnemyTriggerDeath;
}
