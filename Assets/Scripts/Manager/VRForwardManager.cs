﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRForwardManager : MonoBehaviour
{
    public Transform cameraTransform;

    void Start()
    {
        gameObject.transform.position = cameraTransform.position;
    }

    void Update()
    {
        gameObject.transform.rotation = new Quaternion(0.0f, cameraTransform.rotation.y, 0.0f, cameraTransform.rotation.w);
    }
}
