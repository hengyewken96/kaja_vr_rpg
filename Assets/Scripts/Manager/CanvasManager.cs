﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasManager : MonoBehaviour
{
    #region Singleton
    public static CanvasManager instance;

    void Awake()
    {
        if(instance != null)
        {
            Debug.LogWarning("More than one instance found");
            return;
        }

        instance = this;
    }
    #endregion

    [Header("Menu Setting")]
    public Transform reference;
    public Transform mainCam;
    public float inventoryDistance = 0.5f;

    [Header("Canvas")]
    public GameObject infoCanvas;
    public GameObject tabCanvas;
    public GameObject statUI;
    public GameObject equipmentUI;
    public GameObject useItemUI;

    private Vector3 infoPosition;
    private Vector3 activePosition;
    private bool gameStarted = false;

    void Start()
    {
        infoCanvas.SetActive(false);
    }

    void Update()
    {
        CanvasPosition();
        if(gameStarted)
        {
            if(Input.GetKeyDown(KeyCode.I))
            {
                OpenMenu(3);
            }

            if(Input.GetKeyDown(KeyCode.B))
            {
                OpenMenu(2);
            }

            if(Input.GetKeyDown(KeyCode.O))
            {
                OpenMenu(1);
            }
        }
    }

    public void GameStart()
    {
        gameStarted = true;
        infoCanvas.SetActive(true);
    }

    public void OpenMenu(int value)
    {
        switch(value)
        {
            case 1:
                statUI.SetActive(!statUI.activeSelf);
                equipmentUI.SetActive(false);
                useItemUI.SetActive(false);
                break;
            case 2:
                useItemUI.SetActive(!useItemUI.activeSelf);
                statUI.SetActive(false);
                equipmentUI.SetActive(false);
                break;
            case 3:
                equipmentUI.SetActive(!equipmentUI.activeSelf);
                statUI.SetActive(false);
                useItemUI.SetActive(false);
                break;
            default:
                break;
        }
    }
    
    private void CanvasPosition()
    {
        infoPosition = mainCam.position + mainCam.forward * inventoryDistance;
        activePosition = reference.position  + reference.forward * inventoryDistance;
        activePosition.y = mainCam.position.y - 0.05f;

        // Info
        infoCanvas.transform.position = infoPosition;
        infoCanvas.transform.rotation = mainCam.rotation;

        // Inventory
        tabCanvas.transform.position = activePosition;
        tabCanvas.transform.rotation = new Quaternion(0.0f, reference.rotation.y, 0.0f, reference.rotation.w);
    }
}
