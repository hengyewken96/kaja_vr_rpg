﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UseItemManager : MonoBehaviour
{
    #region Singleton
    public static UseItemManager instance;

    void Awake()
    {
        if(instance != null)
        {
            Debug.LogWarning("More than one instance found");
            return;
        }

        instance = this;
    }
    #endregion

    [Header("Active Item Setting")]
    public Text activeText;
    public Text activeDescription;
    
    public Button activeButton;
    private UseItem activeItem = null;

    public delegate void OnItemUsed(UseItem usedItem);
    public OnItemUsed onItemUsed;

    public void ActiveButton(UseItem useItem)
    {
        activeText.text = useItem.name.ToString();
        activeDescription.text = useItem.description.ToString();
        activeItem = useItem;
        activeButton.interactable = true;
    }

    public void ApplyEffect()
    {
        if(activeItem != null)
        {
            if(onItemUsed != null)
            {
                onItemUsed.Invoke(activeItem);
            }

            Inventory.instance.Remove(activeItem);
            activeText.text = "";
            activeDescription.text = "";
            activeItem = null;
            activeButton.interactable = false;
        }
    }
}
