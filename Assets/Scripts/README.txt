----------------------------------------------
 CONTENT
----------------------------------------------
<1> Inventory
<2> Item
<3> Controller

----------------------------------------------
 INVENTORY
----------------------------------------------
<VR>
1. Create a GameObject inside Player object and add VRForwardManager script.
2. Create a GameObject and add CanvasController script & Inventory script.
3. Create a canvas and add InventoryUI script.
4. Inside the canvas, create inventory slots consisting of panels and add InventorySlots script.

<Non VR>
1. Create a GameObject and add CanvasController script & Inventory script.
2. Create a canvas and add InventoryUI script.
3. Inside the canvas, create inventory slots consisting of panels and add InventorySlots script.