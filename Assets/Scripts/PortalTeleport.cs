﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTeleport : MonoBehaviour
{
    public Transform player;
    public Transform receiver;
    private bool playerIsOverlapping = false;

    void Update()
    {
        if(playerIsOverlapping || Input.GetKeyDown(KeyCode.J))
        {
            Vector3 portalToPlayer = player.position - transform.position;
            float dotProduct = Vector3.Dot(transform.up, portalToPlayer);

            // if true: player has moved across the portal
            if(dotProduct < 0f)
            {
                float rotationDiff = -Quaternion.Angle(transform.rotation, receiver.rotation);
                rotationDiff += 180;
                player.Rotate(Vector3.up, rotationDiff);

                Vector3 positionOffset = Quaternion.Euler(0f, rotationDiff, 0f) * portalToPlayer;
                player.position = receiver.position + positionOffset;

                playerIsOverlapping = false;
            }
        }
    }

    void OnCollisionEnter(Collision other)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 1);
        foreach(Collider hit in colliders)
        {
            if(hit.GetComponent<Collider>().tag == "Player")
            {
                playerIsOverlapping = true;
            }
        }
    }

    void OnCollisionExit(Collision other)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 1);
        foreach(Collider hit in colliders)
        {
            if(hit.GetComponent<Collider>().tag == "Player")
            {
                playerIsOverlapping = false;
            }
        }
    }
}
