﻿using UnityEngine;

public class Util
{
    public static void LayerRecursiveSetting(GameObject _obj, int _newLayer)
    {
        if(_obj == null)
        {
            return;
        }

        foreach(Transform _child in _obj.transform)
        {
            if(_child == null)
            {
                continue;
            }

            LayerRecursiveSetting(_child.gameObject, _newLayer);
        }
    }
}
