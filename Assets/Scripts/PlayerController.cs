﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float radius = 2f;
    
    void Update()
    {
        AreaScan();
    }

    // Draw scan area
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
    }

    // Update interactable object within scan radius
    private void AreaScan()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
        foreach(Collider hit in colliders)
        {
            if(hit.GetComponent<Interactable>() != null)
            {
                hit.GetComponent<Interactable>().Interact(transform, radius);
            }
            continue;
        }
    }
}
